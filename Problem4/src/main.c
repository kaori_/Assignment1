/*
 * Description:
 * Keeps receiving a number from input and add the numbers together.
 * The application keeps doing it until the user enter 0.
 * Then it will stop and return the total sum of the numbers the user had entered.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int number = 1;
	int sum = 0;
	printf("We add the numbers you had entered until you enter 0.\n");
	while (number != 0) {
		printf("Input a number (if you want to end, enter 0):\n");
		scanf("%d", &number);
		sum = sum + number;
	}
	printf("Sum : %d", sum);
	return EXIT_SUCCESS;
}
