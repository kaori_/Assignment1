/*
 * Description:
 * Receive two numbers A and B from the input and calculate A to the power of B and print the result.
 */

#include <stdio.h>
#include <stdlib.h>

int calcPower(int a, int b) {
	int pow = 1;
	if (b == 0) {
		return 1;
	}
	for (int i=0; i<b; i++) {
		pow = pow * a;
	}
	return pow;
}

int main(void) {
	int a, b = 0;
	printf("We calculate A to the power of B.\n");
	printf("Input two number (only natural number):\n");
	scanf("%d %d", &a, &b);
	printf("%d ^ %d is %d", a, b, calcPower(a, b));
	return EXIT_SUCCESS;
}
