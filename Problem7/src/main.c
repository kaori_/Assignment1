/*
 * Description:
 * Receives a number from input and find the next prime number which is bigger than the input number.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool checkPrimeNumber(int num) {
	if (num <= 0) {
		return false;
	}
	if (num == 0) {
		return true;
	}
	for (int i=2; i<num; i++) {
		if (num%i == 0) {
			return false;
		}
	}
	return true;
}

int getPrimeNumber(int num) {
	if (num <= 0) {
		return 1;
	}
	do {
		num = num + 1;
	} while (!checkPrimeNumber(num));
	return num;
}

int main(void) {
	int number = 0;
	printf("We print the next prime number.\n");
	printf("Input a number\n");
	scanf("%d", &number);
	printf("A prime number is %d", getPrimeNumber(number));
	return EXIT_SUCCESS;
}
