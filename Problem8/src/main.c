/*
 * Description:
 * Receive two numbers A and B from the input and calculate the quotient without /.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool checkMinus(int num) {
	if (num < 0) {
		return true;
	}
	return false;
}

bool calcQuotient(int a, int b, int *quo) {
	bool isMinus = false;
	int ans = 0;
	if ((a != 0) && (b == 0)) {
		printf("The operation is not possible because one number if zero\n");
		return false;
	}
	if (a == 0) {
		*quo = 0;
		return true;
	}
	if (checkMinus(a)) {
		a = a * (-1);
		isMinus = !isMinus;
	}
	if (checkMinus(b)) {
		b = b * (-1);
		isMinus = !isMinus;
	}
	while (a >= b) {
		a = a - b;
		ans = ans + 1;
	}
	if (isMinus) {
		ans = ans * (-1);
	}
	*quo = ans;
	return true;
}

int main(void) {
	int a, b, quotient = 0;
	printf("We calculate the quotient.\n");
	printf("Input two number:\n");
	scanf("%d %d", &a, &b);
	if (calcQuotient(a, b, &quotient)) {
		printf("%d / %d is %d", a, b, quotient);
	}
	return EXIT_SUCCESS;
}

