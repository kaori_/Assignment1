/*
 * Description:
 * Receive a number for the input and check whether the number is a prime number or not.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool checkPrimeNumber(int num) {
	if (num <= 0) {
		return false;
	}
	if (num == 0) {
		return true;
	}
	for (int i=2; i<num; i++) {
		if (num%i == 0) {
			return false;
		}
	}
	return true;
}

int main(void) {
	int number = 0;
	bool isPrime = false;

	printf("We check whether the number is a prime number or not.\n");
	printf("Input a number:\n");
	scanf("%d", &number);
	isPrime = checkPrimeNumber(number);

	if (isPrime) {
		printf("Your number %d is a prime number", number);
	} else {
		printf("Your number %d is NOT a prime number", number);
	}
	return EXIT_SUCCESS;
}
