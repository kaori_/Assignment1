/*
 * Description:
 * Take 2 operands (number) and one operator (plus, minus, multiplication and devision) and
 * apply the operator on the operands and print the result.
 */

#include <stdio.h>
#include <stdlib.h>

void calc(int a, int b, int ope) {
	switch(ope) {
	case 1:
		printf("%d + %d = %d",a, b, a+b);
		break;
	case 2:
		printf("%d - %d = %d",a, b, a-b);
		break;
	case 3:
		printf("%d * %d = %d",a, b, a*b);
		break;
	case 4:
		if ((a != 0) && (b == 0)) {
			printf("The operation is not possible because one number if zero.");
			break;
		}
		printf("%d / %d = %d",a, b, a/b);
		break;
	default:
		printf("The operator does not exist.");
		break;
	}
}

int main(void) {
	int a, b, operator = 0;
	printf("Which operator do you use?\n");
	printf("1:plus 2:minus 3:multi 4:devi\n");
	scanf("%d", &operator);
	printf("Input two number:\n");
	scanf("%d %d", &a, &b);
	calc(a, b, operator);

	return EXIT_SUCCESS;
}
