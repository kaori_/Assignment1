/*
 * Description:
 * Receive a number from the input and print a stair of shape(*).
 */

#include <stdio.h>
#include <stdlib.h>

void printStar ( int num ) {
	if (num > 0) {
		for(int i=0; i<num; i++) {
			printf("*");
		}
		printf("\n");
	}
}

void printShape ( int num ) {
	for (int i=num; i>0; i--) {
		printStar(i);
	}
}

int main(void) {
	int numberofStars = 0;
	printf("Input a number of stars\n");
	scanf("%d", &numberofStars);
	printShape(numberofStars);
	return EXIT_SUCCESS;
}
