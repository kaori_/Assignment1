/*
 * Description:
 * Receives a number from input and print the sum of the number’s digits.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int number = 1;
	int sum = 0;
	printf("We print the sum of the number’s digits.\n");
	printf("Input a number:\n");
	scanf("%d", &number);
	if (number < 0) {
		number = number * (-1);
	}
	while (number != 0) {
		sum = sum + number % 10;
		number = number / 10;
	}
	printf("Sum : %d", sum);
	return EXIT_SUCCESS;
}
